package servletDispatch;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DispatchServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5515486748857595327L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws javax.servlet.ServletException, java.io.IOException {
		String status = req.getParameter("status");
		int orgFee = 500;

		if (status != null && status.equals("student")) {
			int studentFee = orgFee / 2;
			req.setAttribute("regFee", "" + studentFee);
			req.getRequestDispatcher("/output.jsp").forward(req, res);
			return;
		}
		else if (status != null && status.equals("faculty")) {
			int facultyFee = orgFee;
			req.setAttribute("regFee", "" + facultyFee);
			req.getRequestDispatcher("/output.jsp").forward(req, res);
			return;
		}
	}
}